<?php


namespace App\Controller;

use App\Entity\Categorie;
use App\Entity\Reservation;
use App\Entity\SalleReservable;
use App\Form\creecategorie;
use App\Form\SallesReservable;
use App\Form\TarifType;
use App\Repository\SallesReservableRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CategorieController extends AbstractController
{

    public function categories()
    {
        $categories = $this->getDoctrine()->getRepository(categorie::class)->findAll();
        return $this->render('categorie.html.twig', ['titre' => "Liste des Categories", 'categories' => $categories]);
    }

    public function afficherCategories()
    {
        $categories = $this->getDoctrine()->getRepository(categorie::class)->findAll();
        return $this->render('categorie.html.twig',['titre'=>"Liste des categories",'categories'=>$categories]);
    }

    /**
     * @param Request $request
     * @return Response
     * @IsGranted ("ROLE_SECRETAIRE")
     */
    public function Modtarif(Request $request){

            $FormTarif = $this->createFormBuilder()
                ->add('categorie',EntityType::class,[
                    'class' =>Categorie::class, 'label'=> "Catégorie: "])
                ->add('tarif')
                ->add('enregistrer', SubmitType::class)
                ->getForm();
            $FormTarif->handleRequest($request); // $request est un objet transmis en paramètre de la fonction

            if ($FormTarif->isSubmitted()) {

                $entityManager = $this->getDoctrine()->getManager();

                $cat = $this->getDoctrine()->getRepository(Categorie::class)->find($FormTarif->getData()['categorie']->getId());
                $cat->setTarif($FormTarif->getData()['tarif']);

                $entityManager->flush(); // synchronisation avec la BDD -> production d'un ordre SQL de type INSERT

    }
        // Construction de la réponse du formulaire non soumis -- affichage du formul
        return $this->render('formModTarif.html.twig', ['title'=>'Liste des Catégorie','formTarif' => $FormTarif->createView()]);//A compléter
}
    Public function tarif()

    {
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();
        return $this->render('formModTarif.html.twig',['titre'=>"Modification Tarif ",'categorie'=>$categories]);

    }
    Public function  creationcategorie (Request $request)
{


    $categorie = new Categorie();
    $Formcategorie = $this->createForm(creecategorie::class,$categorie);

    $Formcategorie->handleRequest($request); // $request est un objet transmis en paramètre de la fonction


    if ($Formcategorie->isSubmitted() &&  $Formcategorie->isValid()) {


        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($categorie);
        $entityManager->flush();


    }

// Construction de la réponse du formulaire non soumis -- affichage du formulaire
    return $this->render('security/creecategorie.html.twig', ['Formcategorie' => $Formcategorie->createView()]);//A compléter

}
}