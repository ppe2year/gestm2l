<?php


namespace App\Controller;


use App\Entity\Categorie;
use App\Entity\SalleReservable;
use App\Form\SallesReservable;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormTypeInterface;

class SalleResCat extends AbstractController
{

    public function affectationSalleResaCat(Request $request){

        $formSalleResCat = $this->createFormBuilder()
            ->add('sallereservable',EntityType::class,[
                'class' =>SalleReservable::class, 'label'=> "Salle: "])
            ->add('categorie',EntityType::class,[
                'class' =>Categorie::class, 'label'=> "Catégorie: "])
            ->add('Affecter', SubmitType::class)
            ->getForm();

        $formSalleResCat ->handleRequest($request);


        if ($formSalleResCat->isSubmitted() &&  $formSalleResCat->isValid()){  //ce code est exécuté lors de la soumission du formulaire

            $entityManager = $this->getDoctrine()->getManager();

            $SalleReservable = $this->getDoctrine()->getRepository(SalleReservable::class)->find($formSalleResCat->getData()['sallereservable']);
            $categorie = $this->getDoctrine()->getRepository(Categorie::class)->find($formSalleResCat->getData()['categorie']);

            $SalleReservable->setIdcategorie($categorie);

            $entityManager->flush();
        }

        return $this->render('reservation/sallereservation.html.twig', ['title'=>'Affecter une catégorie','formSalleResCat' => $formSalleResCat->createView()]);
    }
}