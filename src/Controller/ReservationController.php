<?php

namespace App\Controller;

use App\Entity\Reservation;
use App\Entity\User;
use App\Form\ResaType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ReservationController extends AbstractController
{
    /**
     * @Route("/reservation", name="reservation")
     */
    public function index(): Response
    {
        return $this->render('reservation/index.html.twig', [
            'controller_name' => 'ReservationController',
        ]);
    }

    /**
     * @Route("/newreservation",name="newresa")
     * @param Request $request
     * @return RedirectResponse|Response
     * @IsGranted ("ROLE_RESPLIGUES")
     */
    public function addResaSalle(Request $request){
        $idstructure = $this->getUser()->getIdstructures();
        $reservation = new Reservation();
        $reservation->setStructure($idstructure);
        $reservation->setType("Salle Reservable");
        $reservation->setEtat("En attente");
        $formResa = $this->createForm(ResaType::class,$reservation);

        $formResa->handleRequest($request);
        if ($formResa->isSubmitted() && $formResa->isValid()){
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($reservation);
            $entityManager->flush();

            return $this->redirectToRoute('index');
        }
        return $this->render('reservation/reservationsalle.html.twig',array('form'=>$formResa->createView()));
    }

}
