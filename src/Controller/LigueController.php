<?php

namespace App\Controller;

use App\Entity\Bureau;
use App\Entity\Discipline;
use App\Entity\Reservation;
use App\Entity\Salle;
use App\Entity\Structures;
use App\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class LigueController extends AbstractController
{
    /**
     * @Route("/ligue", name="ligue")
     * @IsGranted("ROLE_RESPLIGUES")
     */
    public function maLigue(Request $request): Response
    {

        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(array('email' => $this->getUser()->getUsername()));
        $ligue = $this->getDoctrine()->getRepository(Structures::class)->find($user->getIdstructures());

        $entityManager = $this->getDoctrine()->getManager();

        $ligueInfos = $this->createFormBuilder()
            ->add('nom',TextType::class,[
                'label'=> "Nom: ",
                'data'=> $ligue->getNom()])
            ->add('adresse',TextType::class,[
                'label'=> "Adresse: ",
                'data'=> $ligue->getAdresse()])
            ->getForm()
        ;
        if($ligue->getIdbureau() != null) { $ligueInfos->add('bureau',TextType::class,['label'=> "Bureau: ", 'data'=> $ligue->getIdbureau()->getNom(), 'disabled'=> true]);}
        $ligueInfos->add('discipline',EntityType::class,['class'=> Discipline::class,'data'=> $ligue->getIddiscipline()]);
        $ligueInfos->add('Mettre a jour', SubmitType::class);

        $ligueInfos->handleRequest($request);

        if ($ligueInfos->isSubmitted()  && $ligueInfos->isValid()) {

            if ($ligueInfos->getData()['nom'] != $ligue->getNom()){
                $ligue->setNom($ligueInfos->getData()['nom']);
            }
            if ($ligueInfos->getData()['adresse'] != $ligue->getAdresse()){
                $ligue->setAdresse($ligueInfos->getData()['adresse']);
            }
            if ($ligueInfos->getData()['discipline'] != $ligue->getIddiscipline()){
                $ligue->setIddiscipline($ligueInfos->getData()['discipline']);
            }

            $entityManager->flush();

        }

        $ligueInfos = $this->createFormBuilder()
            ->add('nom',TextType::class,[
                'label'=> "Nom: ",
                'data'=> $ligue->getNom()])
            ->add('adresse',TextType::class,[
                'label'=> "Adresse: ",
                'data'=> $ligue->getAdresse()])
            ->getForm()
        ;

        if($ligue->getIdbureau() != null) {
            $bureauetat = 2;
            $ligueInfos->add('bureau',TextType::class,['label'=> "Bureau: ", 'data'=> $ligue->getIdbureau()->getNom(), 'disabled'=> true]);
        } else if (count($this->getDoctrine()->getRepository(Reservation::class)->checkDemandeBureau($ligue->getId(),'Bureau')) != 0) {
            $bureauetat = 1;
        } else {
            $demandeBureau = $this->createFormBuilder()->add('Demander un bureau', SubmitType::class)->getForm();
            $demandeBureau->handleRequest($request);
            $bureauetat = 0;
        }
        $ligueInfos->add('discipline',EntityType::class,['class'=> Discipline::class,'data'=> $ligue->getIddiscipline()]);
        $ligueInfos->add('Mettre a jour', SubmitType::class);

        $idstructure = $this->getUser()->getIdstructures();
        $demandes = $this->getDoctrine()->getRepository(Reservation::class)->findBy(['structure'=>$idstructure]);

        if (isset($demandeBureau)) {
            if ($demandeBureau->isSubmitted()  && $demandeBureau->isValid()){
                $reserv = new Reservation();
                $reserv->setStructure($ligue);
                $reserv->setType("Bureau");
                $reserv->setEtat("En attente");

                $entityManager->persist($reserv);
                $entityManager->flush();

                $bureauetat = 1;
                $demandes = $this->getDoctrine()->getRepository(Reservation::class)->findBy(['structure'=>$idstructure]);
                return $this->render('ligue/index.html.twig', ['title' => 'Ma ligue','ligueInfos' => $ligueInfos->createView(),'lesdemandes'=>$demandes,'bureauetat' => $bureauetat]);
            } else {
                return $this->render('ligue/index.html.twig', ['title' => 'Ma ligue','ligueInfos' => $ligueInfos->createView(),'demandeBureau' => $demandeBureau->createView(),'lesdemandes'=>$demandes,'bureauetat' => $bureauetat]);
            }
        } else {
            return $this->render('ligue/index.html.twig', ['title' => 'Ma ligue','ligueInfos' => $ligueInfos->createView(),'lesdemandes'=>$demandes,'bureauetat' => $bureauetat]);
        }
    }
}
