<?php

namespace App\Controller;

use App\Entity\Bureau;
use App\Entity\Reservation;
use App\Entity\Structures;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class BureauController extends AbstractController
{
    /**
     * @Route("/bureau", name="bureau")
     */
    public function BureauIndex(): Response
    {
        return $this->render('bureau/index.html.twig', ['title' => 'Gestion des bureaux',]);
    }

    /**
     * @Route("/bureaudisponible", name="bureaudisponible")
     * @IsGranted("ROLE_SECRETAIRE")
     */
    public function ListeBureauDispo(Request $request): Response
    {
        $formRechAut = $this->createFormBuilder()
            ->add('bureau',EntityType::class,[
                'class'=> Bureau::class,
                'label'=> "Bureau: ",
                'choices'=>  $this->getDoctrine()->getRepository(Bureau::class)->getLibre()])
            ->add('structures',EntityType::class,[
                'label'=> "Structures: ",
                'class'=> Structures::class])
            ->add('Ajouter', SubmitType::class)
            ->getForm()
        ;

        $formRechAut->handleRequest($request);

        if ($formRechAut->isSubmitted()  && $formRechAut->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();

            $bureau = $this->getDoctrine()->getRepository(Bureau::class)->find($formRechAut->getData()['bureau']->getId());
            $bureau->setOccupant($formRechAut->getData()['structures']->getId());
            $entityManager->flush();

        }

        $formLiberer = $this->createFormBuilder()
            ->add('bureau_2',EntityType::class,[
                'class'=> Bureau::class,
                'label'=> "Bureau: ",
                'choices'=>  $this->getDoctrine()->getRepository(Bureau::class)->getNonLibre() ])
            ->add('Liberer', SubmitType::class)
            ->getForm()
        ;

        $formLiberer->handleRequest($request);

        if ($formLiberer->isSubmitted()  && $formLiberer->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();

            $bureau = $this->getDoctrine()->getRepository(Bureau::class)->find($formLiberer->getData()['bureau_2']->getId());
            $bureau->setOccupant(null);
            $entityManager->flush();

        }

        $formLiberer = $this->createFormBuilder()
            ->add('bureau_2',EntityType::class,[
                'class'=> Bureau::class,
                'label'=> "Bureau: ",
                'choices'=>  $this->getDoctrine()->getRepository(Bureau::class)->getNonLibre() ])
            ->add('Liberer', SubmitType::class)
            ->getForm()
        ;

        $formRechAut = $this->createFormBuilder()
            ->add('bureau',EntityType::class,[
                'class'=> Bureau::class,
                'label'=> "Bureau: ",
                'choices'=>  $this->getDoctrine()->getRepository(Bureau::class)->getLibre()])
            ->add('structures',EntityType::class,[
                'label'=> "Structures: ",
                'class'=> Structures::class])
            ->add('Ajouter', SubmitType::class)
            ->getForm()
        ;

        $bureaux = $this->getDoctrine()->getRepository(Bureau::class)->getLibre();

        $demandes = $this->getDoctrine()->getRepository(Reservation::class)->getReservationAttente();

        return $this->render('bureau/listedispo.html.twig', ['title' => 'Liste des bureaux disponibles','bureaux'=>$bureaux,'formRechAut' => $formRechAut->createView(),'formLiberer' => $formLiberer->createView(),'lesdemandes'=>$demandes]);
    }
}
