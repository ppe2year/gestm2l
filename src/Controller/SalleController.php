<?php

namespace App\Controller;

use App\Entity\Categorie;
use App\Entity\SalleReservable;
use phpDocumentor\Reflection\Types\Integer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SalleController extends AbstractController
{
    /**
     * @Route("/salles", name="salle")
     * @param Request $request
     * @return Response
     */
    public function listeSalle(Request $request)
    {
        if($request->isXmlHttpRequest()){ //Verification pour savoir si la requête est une requête AJAX
            $categorie = $request->request->get('idcategorie'); //récupération de l'id de la catégorie récupérer par le script ajax dans le fichier listeSalles.html.twig
            //dump($categorie);
            if ($categorie ){ //vérification de l'existance de donnée dans la variable $catégorie
                if (!is_numeric($categorie)){ // si $catégorie n'est pas un nombre
                    $salles = $this->getDoctrine()->getRepository(SalleReservable::class)->findAll();
                }
                else{
                    $salles = $this->getDoctrine()->getRepository(SalleReservable::class)->findBy(['idcategorie'=>$categorie]);
                }
            }
            else {
                $salles = $this->getDoctrine()->getRepository(SalleReservable::class)->findAll();
            }
            return $this->render('salle/listeSallesEmbed.html.twig',['salleReservable'=> $salles]);
        }
        else{
            $salles = $this->getDoctrine()->getRepository(SalleReservable::class)->findAll();
            $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();
            return $this->render('salle/index.html.twig',['title'=>'Liste des salles','salleReservable'=> $salles,'categories'=>$categories]);
        }
    }

    /**
     * @Route("/salle", name="salleResa")
     * @param Request $request
     * @return Response
     */
    public function afficherSallesReservable(Request $request)
    {
        if($request->isXmlHttpRequest()){ //Verification pour savoir si la requête est une requête AJAX
            $categorie = $request->request->get('idcategorie'); //récupération de l'id de la catégorie récupérer par le script ajax dans le fichier listeSalles.html.twig
            //dump($categorie);
            if ($categorie ){ //vérification de l'existance de donnée dans la variable $catégorie
                if (!is_numeric($categorie)){ // si $catégorie n'est pas un nombre
                    $salles = $this->getDoctrine()->getRepository(SalleReservable::class)->findBy(['reseve'=>true]);
                }
                else{
                    $salles = $this->getDoctrine()->getRepository(SalleReservable::class)->findBy(['reseve'=>true,'idcategorie'=>$categorie]);
                }
            }
            else {
                $salles = $this->getDoctrine()->getRepository(SalleReservable::class)->findBy(['reseve'=>true]);
            }
            return $this->render('salle/listeSallesEmbed.html.twig',['salleReservable'=> $salles]);
        }
        else{
            $salles = $this->getDoctrine()->getRepository(SalleReservable::class)->findBy(['reseve'=>true]);
            $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();
            return $this->render('salle/listeSalles.html.twig',['title'=>'Liste des salles réservable','salleReservable'=> $salles,'categories'=>$categories]);
        }
    }





}
