<?php


namespace App\Form;


use App\Entity\Categorie;
use App\Entity\SalleReservable;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SallesReservable extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('sallereservable',EntityType::class,[
                'class' =>SalleReservable::class])
            ->add('libelle',EntityType::class,[
                'class' =>Categorie::class])
            ->add('Affecter', SubmitType::class);


    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Categorie::class,
        ]);
    }
}