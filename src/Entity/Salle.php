<?php

namespace App\Entity;

use App\Repository\SalleRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SalleRepository::class)
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({"salle" = "Salle", "bureau" = "Bureau", "sallereservable" = "SalleReservable"})
 * @ORM\Table(name="salle")
 */
class Salle
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="salle_idsalle_seq")
     * @ORM\Column(type="integer",name="idsalle")
     */
    private $id;

    /**
     * @ORM\Column(type="string",name="nom")
     */
    private $nom;

    /**
     * @ORM\Column(type="integer")
     */
    private $situation;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Etage")
     * @ORM\JoinColumn(name="situation", referencedColumnName="idetage")
     */

    private $etage;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getSituation(): ?int
    {
        return $this->situation;
    }

    public function setSituation(int $situation): self
    {
        $this->situation = $situation;

        return $this;
    }

    public function getEtage(): ?Etage
    {
        return $this->etage;
    }

    public function setEtage(Etage $etage): self
    {
        $this->etage = $etage;
        return $this;
    }
}
