<?php

namespace App\Entity;

use App\Repository\ReservationRepository;
use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\Boolean;

/**
 * @ORM\Entity(repositoryClass=ReservationRepository::class)
 */
class Reservation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\SequenceGenerator(sequenceName="reservation_idreservation_seq")
     * @ORM\Column(type="integer",name="idreservation")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Salle")
     * @ORM\JoinColumn(name="idsalle", referencedColumnName="idsalle")
     */
    private $salle;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Structures")
     * @ORM\JoinColumn(name="idstructure", referencedColumnName="idstructures")
     */
    private $structure;

    /**
     * @ORM\Column(type="string")
     */
    private $type;

    /**
     * @ORM\OneToOne  (targetEntity="App\Entity\Categorie")
     * @ORM\JoinColumn(name="idcategorie", referencedColumnName="idcategorie")
     */
    private $categorie;

    /**
     * @ORM\Column(type="string")
     */
    private $etat;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSalle(): ?Salle
    {
        return $this->salle;
    }

    public function setSalle(?Salle $salle): self
    {
        $this->salle = $salle;

        return $this;
    }

    public function getStructure(): ?Structures
    {
        return $this->structure;
    }

    public function setStructure(?Structures $structure): self
    {
        $this->structure = $structure;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getEtat(): ?string
    {
        return $this->etat;
    }

    public function setEtat(string $etat): self
    {
        $this->etat = $etat;

        return $this;
    }

    public function getIdcategorie(): ?Categorie
    {
        return $this->categorie;
    }

    public function setIdcategorie(Categorie $idcategorie): self
    {
        $this->categorie = $idcategorie;

        return $this;
    }


}
