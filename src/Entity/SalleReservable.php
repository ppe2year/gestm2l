<?php

namespace App\Entity;

use App\Repository\SallesReservableRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @ORM\Entity(repositoryClass=SallesReservableRepository::class)
 * @ORM\Table (name="sallereservable")
 */
class SalleReservable extends Salle
{
    /**
     * @ORM\ManyToOne (targetEntity="App\Entity\Categorie")
     * @ORM\JoinColumn(name="idcategorie", referencedColumnName="idcategorie")
     */
    private $idcategorie;

    /**
     * @ORM\Column(type="boolean",name="etat",options={"default":true})
     */
    private $reseve;

    public function getId(): ?int
    {
        return parent::getId();
    }
    public function getNom(): ?string
    {
        return parent::getNom();
    }
    public function getEtage(): ?Etage
    {
        return parent::getEtage();
    }


    public function getIdcategorie(): ?Categorie
    {
        return $this->idcategorie;
    }

    public function setIdcategorie(?Categorie $idcategorie): self
    {
        $this->idcategorie = $idcategorie;

        return $this;
    }

    public function getReseve(): ?bool
    {
        return $this->reseve;
    }

    public function setReseve(bool $reseve): self
    {
        $this->reseve = $reseve;

        return $this;
    }
    public function __toString()
    {
        return "" . $this->getNom();
    }
}
