<?php

namespace App\Entity;

use App\Repository\DisciplineRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DisciplineRepository::class)
 */
class Discipline
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\SequenceGenerator(sequenceName="discipline_iddiscipline_seq")
     * @ORM\Column(type="integer",name="iddiscipline")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $libelle;

    /**
     * @ORM\Column(type="boolean")
     */
    private $olympique;

    public function __toString()
    {
        return "" . $this->libelle;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getOlympique(): ?bool
    {
        return $this->olympique;
    }

    public function setOlympique(bool $olympique): self
    {
        $this->olympique = $olympique;

        return $this;
    }
}
