<?php

namespace App\Entity;

use App\Repository\EtageRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EtageRepository::class)
 * @ORM\Table (name="etage")
 */
class Etage
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer",name="idetage")
     */
    private $id;

    /**
     * @ORM\Column (type="integer",name="numero")
     */
    private $numero;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Batiment")
     * @ORM\JoinColumn(name="batiment", referencedColumnName="idbatiment")
     */
    private $idbatiment;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumero(): ?int
    {
        return $this->numero;
    }

    public function setNumero(int $numero): self
    {
        $this->numero = $numero;

        return $this;
    }

    public function getIdbatiment(): ?Batiment
    {
        return $this->idbatiment;
    }

    public function setIdbatiment(?Batiment $idbatiment): self
    {
        $this->idbatiment = $idbatiment;

        return $this;
    }
}
