<?php

namespace App\Entity;

use App\Repository\BureauRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BureauRepository::class)
 * @ORM\Table(name="bureau")
 */
class Bureau extends Salle
{
    /**

     * @ORM\Column(type="integer", nullable=true)

     */
    private $occupant;


    public function getId(): ?int
    {
        return parent::getId();
    }

    public function __toString()
    {
        return "" . $this->getNom();
    }

    public function getOccupant(): ?int
    {
        return $this->occupant;
    }

    public function setOccupant(?int $occupant): self
    {
        $this->occupant = $occupant;

        return $this;
    }

}
