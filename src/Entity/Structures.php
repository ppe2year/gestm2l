<?php

namespace App\Entity;

use App\Repository\StructuresRepository;
use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\Boolean;

/**
 * @ORM\Entity(repositoryClass=StructuresRepository::class)
 */
class Structures
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")

     * @ORM\SequenceGenerator(sequenceName="structures_idstructures_seq")

     * @ORM\Column(type="integer",name="idstructures")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Bureau")
     * @ORM\JoinColumn(name="idbureau", referencedColumnName="idsalle")
     */
    private $idbureau;

    /**

     * @ORM\Column(type="string")

     */
    private $nom;

    /**

     * @ORM\Column(type="string")

     */
    private $adresse;

    /**

     * @ORM\Column(type="string")

     */
    private $nomfichier;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Discipline")
     * @ORM\JoinColumn(name="iddiscipline", referencedColumnName="iddiscipline")
     */
    private $iddiscipline;

    public function __toString()
    {
        return "" . $this->nom;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getNomfichier(): ?string
    {
        return $this->nomfichier;
    }

    public function setNomfichier(string $nomfichier): self
    {
        $this->nomfichier = $nomfichier;

        return $this;
    }

    public function getIdbureau(): ?Bureau
    {
        return $this->idbureau;
    }

    public function setIdbureau(?Bureau $idbureau): self
    {
        $this->idbureau = $idbureau;

        return $this;
    }

    public function getIddiscipline(): ?Discipline
    {
        return $this->iddiscipline;
    }

    public function setIddiscipline(?Discipline $iddiscipline): self
    {
        $this->iddiscipline = $iddiscipline;

        return $this;
    }


}
