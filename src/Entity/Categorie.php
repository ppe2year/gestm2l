<?php

namespace App\Entity;

use App\Repository\CategorieRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CategorieRepository::class)
 */
class Categorie
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="categorie_idcategorie_seq")
     * @ORM\Column(type="integer",name="idcategorie")
     */
    private $id;

    /**
     * @ORM\Column(type="string",name="libelle")
     */
    private $libelle;

    /**
     * @ORM\Column(type="float",name="tarif")
     */
    private $tarif;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getTarif(): ?float
    {
        return $this->tarif;
    }

    public function setTarif(float $tarif): self
    {
        $this->tarif = $tarif;

        return $this;
    }
    public function __toString()
    {
        return "" . $this->getLibelle();
    }
}

